#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_Nord.mk

COMMON_LUNCH_CHOICES := \
    omni_Nord-user \
    omni_Nord-userdebug \
    omni_Nord-eng
